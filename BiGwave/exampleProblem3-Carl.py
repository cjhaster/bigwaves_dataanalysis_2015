# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 10:40:41 2015

@author: simons

Start playing around with Carl's freq domain + coloured noise signal

"""
#==============================================================================
#                               LOAD PACKAGES
#==============================================================================
import BiGwave as bw
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import emcee
import corner

root_folder = "/Users/simons/Dropbox/Birmingham/BiG Waves/BiG Waves 2015/BiGwave/"
bitbucket_folder = "/Users/simons/bitbucket/bigwaves_dataanalysis_2015/"
extension = ".pdf"

PSD_data_file = "PSD.dat"
data_file = "coloured_data.dat"
freq_series_data_file = "freqseries.dat"
time_series_data_file = "timeseries.dat"
whitened_time_data_file = "whitened_t_data.dat"

#==============================================================================
#                               LOAD CARLS DATA
#==============================================================================

PSD_data = np.loadtxt(bitbucket_folder + PSD_data_file)
data = np.loadtxt(bitbucket_folder + data_file)
freq_series_data = np.loadtxt(bitbucket_folder + freq_series_data_file)
time_series_data = np.loadtxt(bitbucket_folder + time_series_data_file)
whitened_time_data = np.loadtxt(bitbucket_folder + whitened_time_data_file)

#==============================================================================
#                               PLOT THE PSD
#==============================================================================

figName = "aLIGO design PSD"
plt.figure(figName)
plt.clf()
plt.loglog(freq_series_data, PSD_data)
plt.xlim()
plt.ylim()
plt.xlabel(r"$f$")
plt.ylabel(r"$S$")
plt.savefig(root_folder + figName + extension)

#==============================================================================
#                              PLOT THE DATA
#==============================================================================

figName = "Carls data file"
plt.figure(figName)
plt.clf()
plt.plot(time_series_data, data)
plt.xlim()
plt.ylim()
plt.xlabel(r"$t$")
plt.ylabel(r"$h$")
plt.savefig(root_folder + figName + extension)

figName = "Carls whitened data file"
plt.figure(figName)
plt.clf()
plt.plot(time_series_data, whitened_time_data)
plt.xlim()  
plt.ylim()
plt.xlabel(r"$t$")
plt.ylabel(r"$h$")
plt.savefig(root_folder + figName + extension)


plt.show()
plt.close('all')

#==============================================================================
#                                   NOTES
#==============================================================================

