# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 12:59:46 2015

@author: simons

BiG waves code

"""
#==============================================================================
#                           LOAD PACKAGES
#==============================================================================
import numpy as np

#==============================================================================
#                               PRIOR RANGES FOR WAVELETS
#==============================================================================

A_min = 0.0
A_max = 2.0
f0_min = 0.0
f0_max = 10.0
Q_min = 1.0
Q_max = 100.0
t0_min = -2.0
t0_max = 2.0
phi0_min = 0.0
phi0_max = 2.0*np.pi
    
#==============================================================================
#                           SOME USEFUL FUNCTIONS
#==============================================================================

def waveletTimeDomain(t, A, f0, Q, t0, phi0):
    """
    Morlet-Gabor sine-Gaussian wavelet in the time domain
    Equation 4 in http://arxiv.org/abs/1410.3835v3
    
    Parameters
    -----------
    t : float
        time
    A : float
        Amplitude
    f0 : float
        Central frequency
    Q : float
        Quality factor
    t0 : float
        Central time
    phi0 : float
        Central frequency
    
    Returns
    --------
    psi : float
        Amplitude of the wavelet at time t
    
    -- Simon Stevenson
    """
    tau = Q / (2.0*np.pi*f0)
    dt = t-t0
    firstTerm = np.exp(-(dt*dt)/(tau*tau))
    secondTerm = np.cos((2.0*np.pi*f0*dt) + phi0)
    return A * firstTerm * secondTerm
    
def waveletFreqDomain(f, A, f0, Q, t0, phi0):
    """
    Morlet-Gabor sine-Gaussian wavelet in the freq domain
    Equation 5 in http://arxiv.org/abs/1410.3835v3
    
    Parameters
    -----------
    f : float
        Frequency
    A : float
        Amplitude
    f0 : float
        Central frequency
    Q : float
        Quality factor
    t0 : float
        Central time
    phi0 : float
        Central frequency
    
    Returns
    --------
    psi : float
        Amplitude of the wavelet at time t
    
    -- Simon Stevenson
    """
    tau = Q / (2.0*np.pi*f0)
    df = f-f0
    dfp = f+f0
    prefactor = (np.sqrt(np.pi)*A*tau)/(2.0)
    firstTerm = np.exp(-np.pi*np.pi*tau*tau*df*df)
    secondTerm = np.exp(j*(phi0 + 2.0*np.pi*df*t0))
    thirdTerm = np.exp(-j*(phi0 + 2.0*np.pi*dfp*t0)) * np.exp(-(Q*Q*f)/f0)
    return prefactor * firstTerm * (secondTerm + thirdTerm)
    
#-- what else do we need here? 
#-- noise weighted inner product?
    
    
    

