# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 13:01:16 2015

@author: simons

problem 1 example solution using the BiGwave package

"""
#==============================================================================
#                               LOAD PACKAGES
#==============================================================================
import BiGwave as bw
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import emcee
import corner

root_folder = "/Users/simons/bitbucket/bigwaves_dataanalysis_2015/BiGwave/"
extension = ".pdf"

#==============================================================================
#                           DEFINE A LIKELIHOOD FOR EMCEE
#==============================================================================

#def drawParams(nWavelets=1, nDraws=1):
#    """
#    Draw the parameters for each wavelet from the prior
#    The parameters for each wavelet are A, f0, Q, t0, phi0
#
#    Parameters
#    ------------
#    nWavelets : int
#        How many wavelets to use for the reconstruction
#    nDraws : int
#        How many walkers to use in the mcmc
#
#    Returns
#    ---------    
#    wavelet_params : array
#        Array of wavelet params nDraws * nWavelets * nparams
#    
#    -- Simon Stevenson
#    """
#    wavelet_params = np.zeros((nDraws, nWavelets, nparams))
#    #-- draw A from between Amin, Amax
#    wavelet_params[:, :, 0] = np.random.rand(nDraws, nWavelets) * (bw.A_max - bw.A_min) + bw.A_min
#    #-- draw f0
#    wavelet_params[:, :, 1] = np.random.rand(nDraws, nWavelets) * (bw.f0_max - bw.f0_min) + bw.f0_min
#    #-- draw Q
#    wavelet_params[:, :, 2] = np.random.rand(nDraws, nWavelets) * (bw.Q_max - bw.Q_min) + bw.Q_min
#    #-- draw t0
#    wavelet_params[:, :, 3] = np.random.rand(nDraws, nWavelets) * (bw.t0_max - bw.t0_min) + bw.t0_min
#    #-- draw phi0
#    wavelet_params[:, :, 4] = np.random.rand(nDraws, nWavelets) * (bw.phi0_max - bw.phi0_min) + bw.phi0_min
#    return wavelet_params

#def lnprior(theta):
#    """
#    Calculate the log of the prior probability for this combination of parameters 
#    Currently all just flat priors within an allowed range    
#    
#    Parameters
#    ------------    
#    theta : array
#        Array containing model parameters
#    
#    Returns
#    ---------
#    lnprior : float
#        log of the prior probability
#        
#    -- Simon Stevenson 
#    """
#    #-- theta has shape nwavelets * nparams
#    nwavelets = theta.shape[0]
#    nparams = theta.shape[1]
#    
#    print theta.shape, theta
#    
#    A_p = theta[:, 0]
#    f0_p = theta[:, 1]
#    Q_p = theta[:, 2]
#    t0_p = theta[:, 3]
#    phi0_p = theta[:, 4]
#    
#    #-- check if within allowed prior
#    A_ok = np.logical_and(A_p > bw.A_min, A_p < bw.A_max)
#    f0_ok = np.logical_and(f0_p > bw.f0_min, f0_p < bw.f0_max)
#    Q_ok = np.logical_and(Q_p > bw.Q_min, Q_p < bw.Q_max)
#    t0_ok = np.logical_and(t0_p > bw.t0_min, t0_p < bw.t0_max)
#    phi0_ok = np.logical_and(phi0_p > bw.phi0_min, phi0_p < bw.phi0_max)
#    
##    print A_ok.all(), f0_ok.all(), Q_ok.all(), t0_ok.all(), phi0_ok.all()
#    
#    if(A_ok.all() and f0_ok.all() and Q_ok.all() and t0_ok.all() and phi0_ok.all()):
#        return 0
#    else:
#        return -np.inf
#    
#def lnlike(theta): #, data
#    """
#    Calculate the log likelihood for this combination of parameters
#    
#    Parameters
#    ------------
#    theta : array 
#        Array containin the parameter choices
#    data : array
#        Array containing the data
#        
#    Returns
#    --------
#    lnlike : float
#        log likelihood
#        
#    -- Simon Stevenson
#    """
#    nwavelets = theta.shape[0]
#    nparams = theta.shape[1]    
#    
#    print theta.shape, theta
#
#    A_p = theta[:, 0]
#    f0_p = theta[:, 1]
#    Q_p = theta[:, 2]
#    t0_p = theta[:, 3]
#    phi0_p = theta[:, 4]
#
##    A_p, f0_p, Q_p, t0_p, phi0_p = theta
#    #== this currently uses a global definition of t, probably want to 
#    wavelet_test = np.zeros((nwavelets, len(t)))
#
#    for i in range(nwavelets):
#        wavelet_test[i, :] = bw.waveletTimeDomain(t, A_p[i], f0_p[i], Q_p[i], t0_p[i], phi0_p[i])
#
#    residuals = data - sum(wavelet_test)
#    return np.sum(scipy.stats.norm(0,Anoise).logpdf(residuals))
#    
#def lnprob(theta): #, data
#    """
#    Calculate the log posterior probability given this combination of parameters
#    
#    Parameters
#    -----------
#    theta : array
#        Array containing the parameter choices
#    data : array
#        Array containing the data
#    
#    Returns
#    --------
#    lnprob : float
#        log posterior probability
#        
#    -- Simon Stevenson
#    """
#    lp = lnprior(theta)
#    print "lp=", lp
#    if not np.isfinite(lp):
##        print "rejected point" #-- if a lot are rejected then maybe you need to be smarter about sampling -- see stan
#        return -np.inf
#    print lp + lnlike(theta)
#    return lp + lnlike(theta) #, data

#==============================================================================
#               CAN YOU TRY AND SIMPLIFY THESE AND JUST FIT ONE WAVELET
#==============================================================================

def drawParams(nWavelets=1, nDraws=1):
    """
    Draw the parameters for each wavelet from the prior
    The parameters for each wavelet are A, f0, Q, t0, phi0

    Parameters
    ------------
    nWavelets : int
        How many wavelets to use for the reconstruction
    nDraws : int
        How many walkers to use in the mcmc

    Returns
    ---------    
    wavelet_params : array
        Array of wavelet params nDraws * nWavelets * nparams
    
    -- Simon Stevenson
    """
    #== assume nWavelets = 1 for now
    wavelet_params = np.zeros((nDraws, nparams))
    #-- draw A from between Amin, Amax
    wavelet_params[:, 0] = np.random.rand(nDraws) * (bw.A_max - bw.A_min) + bw.A_min
    #-- draw f0
    wavelet_params[:, 1] = np.random.rand(nDraws) * (bw.f0_max - bw.f0_min) + bw.f0_min
    #-- draw Q
    wavelet_params[:, 2] = np.random.rand(nDraws) * (bw.Q_max - bw.Q_min) + bw.Q_min
    #-- draw t0
    wavelet_params[:, 3] = np.random.rand(nDraws) * (bw.t0_max - bw.t0_min) + bw.t0_min
    #-- draw phi0
    wavelet_params[:, 4] = np.random.rand(nDraws) * (bw.phi0_max - bw.phi0_min) + bw.phi0_min
    return wavelet_params

def lnprior(theta):
    """
    Calculate the log of the prior probability for this combination of parameters 
    Currently all just flat priors within an allowed range    
    
    Parameters
    ------------    
    theta : array
        Array containing model parameters
    
    Returns
    ---------
    lnprior : float
        log of the prior probability
        
    -- Simon Stevenson 
    """
    #-- theta has shape nwavelets * nparams
#    nwavelets = theta.shape[0] #-- assume nwavelets =1
    nparams = len(theta)#theta.shape[1]
    
#    print theta.shape, theta
    
    A_p, f0_p, Q_p, t0_p, phi0_p = theta
#    A_p = theta[:, 0]
#    f0_p = theta[:, 1]
#    Q_p = theta[:, 2]
#    t0_p = theta[:, 3]
#    phi0_p = theta[:, 4]
    
    #-- check if within allowed prior
    A_ok = np.logical_and(A_p > bw.A_min, A_p < bw.A_max)
    f0_ok = np.logical_and(f0_p > bw.f0_min, f0_p < bw.f0_max)
    Q_ok = np.logical_and(Q_p > bw.Q_min, Q_p < bw.Q_max)
    t0_ok = np.logical_and(t0_p > bw.t0_min, t0_p < bw.t0_max)
    phi0_ok = np.logical_and(phi0_p > bw.phi0_min, phi0_p < bw.phi0_max)
    
#    print A_ok.all(), f0_ok.all(), Q_ok.all(), t0_ok.all(), phi0_ok.all()
    
    if(A_ok.all() and f0_ok.all() and Q_ok.all() and t0_ok.all() and phi0_ok.all()):
        return 0
    else:
        return -np.inf
    
def lnlike(theta): #, data
    """
    Calculate the log likelihood for this combination of parameters
    
    Parameters
    ------------
    theta : array 
        Array containin the parameter choices
    data : array
        Array containing the data
        
    Returns
    --------
    lnlike : float
        log likelihood
        
    -- Simon Stevenson
    """
#    nwavelets = theta.shape[0]
    nparams = len(theta)#.shape[1]    
    
#    print theta.shape, theta

    A_p, f0_p, Q_p, t0_p, phi0_p = theta

#    A_p = theta[:, 0]
#    f0_p = theta[:, 1]
#    Q_p = theta[:, 2]
#    t0_p = theta[:, 3]
#    phi0_p = theta[:, 4]

#    A_p, f0_p, Q_p, t0_p, phi0_p = theta
    #== this currently uses a global definition of t, probably want to 
#    wavelet_test = np.zeros((nwavelets, len(t)))

#    for i in range(nwavelets):
#        wavelet_test[i, :] = bw.waveletTimeDomain(t, A_p[i], f0_p[i], Q_p[i], t0_p[i], phi0_p[i])

    wavelet_test = bw.waveletTimeDomain(t, A_p, f0_p, Q_p, t0_p, phi0_p)

    residuals = data - wavelet_test #-- sum up wavelets when using more than one
    return np.sum(noise_distribution.logpdf(residuals)) #-- sum up logs
    
def lnprob(theta): #, data
    """
    Calculate the log posterior probability given this combination of parameters
    
    Parameters
    -----------
    theta : array
        Array containing the parameter choices
    data : array
        Array containing the data
    
    Returns
    --------
    lnprob : float
        log posterior probability
        
    -- Simon Stevenson
    """
    lp = lnprior(theta)
    #print "lp=", lp
    if not np.isfinite(lp):
#        print "rejected point" #-- if a lot are rejected then maybe you need to be smarter about sampling -- see stan
        return -np.inf
    #print lp + lnlike(theta)
    return lp + lnlike(theta) #, data

#==============================================================================
#                        PLOT AN EXAMPLE WAVELET
#==============================================================================

#-- define the timeseries
tmin = -2.0
tmax = 2.0
ntimepoints = 1000
t = np.linspace(tmin,tmax,ntimepoints)

#-- define the wavelet parameters
A=0.8
f0=7.1
Q=11.8
t0=0.2
phi0=np.pi/4.0
injected_wavelet_params = np.array([A, f0, Q, t0, phi0])
wavelet = bw.waveletTimeDomain(t,A,f0,Q,t0,phi0)

#-- save the wavelet used to a txt file, time against amplitude
header_string = "t\tpsi\nA=%f,f0=%f,Q=%f,t0=%f,phi0=%f" % (A,f0,Q,t0,phi0)
np.savetxt(root_folder + "injected_wavelet_1.txt", np.transpose((t, wavelet)), header=header_string)

figName = "1) example wavelet"
plt.figure(figName)
plt.clf()
plt.plot(t, wavelet)
plt.xlabel(r"time (s)")
plt.ylabel(r"signal(t)")
plt.savefig(root_folder + figName + extension)

#==============================================================================
#                           GENERATE SOME GAUSSIAN NOISE
#==============================================================================
#generate some gaussian noise
mean_noise = 0.0
Anoise = 1.0
noise_distribution = scipy.stats.norm(mean_noise, Anoise)
noise = noise_distribution.rvs(ntimepoints)
#noise = np.zeros(ntimepoints)

figName = "2) example noise"
plt.figure(figName)
plt.clf()
plt.plot(t, noise)
plt.xlabel(r"time (s)")
plt.ylabel(r"noise(t)")
plt.savefig(root_folder + figName + extension)

#==============================================================================
#                           GENERATE SOME DATA
#==============================================================================

data = wavelet + noise

np.savetxt(root_folder + "example_data_1.txt", np.transpose((t, data)), header="t\td(t)")

figName = "3) example data"
plt.figure(figName)
plt.clf()
plt.plot(t, data)
plt.xlabel(r"time (s)")
plt.ylabel(r"data (t)")
plt.savefig(root_folder + figName + extension)

#==============================================================================
#               GENERATE A TEMPLATE AND SUBTRACT IT FROM THE DATA
#==============================================================================

#-- need to put this thing in a loop/emcee
A2=1.01
f02=1.01
Q2=4.01
t02=0.01
phi02=0.01
template_wavelet_params = np.array([A2, f02, Q2, t02, phi02])
wavelet2 = bw.waveletTimeDomain(t, A2, f02, Q2, t02, phi02)
residuals = data - wavelet2

figName = "4) example wavelet subtracted from data"
plt.figure(figName)
plt.clf()
plt.plot(t, wavelet2)
plt.xlabel(r"time (s)")
plt.ylabel(r"template(t)")
plt.savefig(root_folder + figName + extension)

figName = "5) example residuals"
plt.figure(figName)
plt.clf()
plt.plot(t, residuals)
plt.xlabel(r"time (s)")
plt.ylabel(r"residuals(t)")
plt.savefig(root_folder + figName + extension)

#==============================================================================
#                       CALCULATE PROBABILITY OF RESIDUALS
#==============================================================================

likelihood_residuals = noise_distribution.pdf(residuals)
log_likelihood_residuals = noise_distribution.logpdf(residuals)
total_log_likelihood = np.sum(log_likelihood_residuals)

print "log_L = ", total_log_likelihood

#==============================================================================
#                       DO THIS USING EMCEE
#==============================================================================

nparams     = 5                         #-- Number of parameters for EACH wavelet
nwavelets   = 1                         #-- Number of wavelets to use in the recovery (is this the correct way to try to fit more than one? I think so)
ndim        = nparams * nwavelets       #-- number of parameters to search
nwalkers    = 200                       #-- number of walkers
nburn       = 200                       #-- number of steps in burn in period
nsteps      = 500#1000                  #-- number of MCMC steps to take (total no. of samples = (nsteps - nburn) * nwalkers) 

#-- draw the parameters for the wavelets from the prior
initialGuessAtParams = drawParams(nWavelets=nwavelets, nDraws=nwalkers)  

sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob)

print "Starting emcee"

sampler.run_mcmc(initialGuessAtParams, nsteps) 

print "Finished running emcee"

samples = sampler.chain[:, nburn:, :].reshape((-1, ndim))

#np.savetxt(plots_folder + "posterior_samples.txt", samples)

makePlots=True

parameter_list = ["A", "f0", "Q", "t0", "phi0"]

if(makePlots):
    
    #-- work out how to plot the chain
    for i in range(nparams):
        figName = parameter_list[i] + " chains"
        plt.figure(figName)
        plt.clf()
        plt.plot(sampler.chain[:, :, i].T, color='k')
        plt.xlabel("Step number")
        plt.ylabel(parameter_list[i])        
        plt.savefig(root_folder + figName + extension)

        figName = parameter_list[i] + " hist"
        plt.figure(figName)
        plt.clf()
        plt.hist(samples[:, i], bins=50, normed=True, histtype='step')
        plt.xlabel(parameter_list[i])
        plt.ylabel("p(" + parameter_list[i] + ")")        
        plt.savefig(root_folder + figName + extension)
        
    figName = "triangle_plot"
#    theLabels = []
#    for i in range(nparams):
#        theLabels.append("%d" % i)
    fig = corner.corner(samples, labels=parameter_list, truths=injected_wavelet_params) # ,truths=[lamda_guess, sigma_xx_guess, sigma_yy_guess])
    fig.savefig(root_folder + figName + extension)
    
    ln_likes=np.zeros(len(samples))    
    for i in range(len(samples)):
        ln_likes[i] = lnlike(samples[i])
    max_likelihood_params = samples[np.argmax(ln_likes)]  
    
    #-- plots some fair draws from the posterior of the wavelet over the data
    figName = "maximum likelihood wavelet"
    plt.figure(figName)
    plt.clf()
    plt.plot(t, data, c='gray', alpha=0.7)
    plt.plot(t, wavelet, c='r')
    plt.plot(t, bw.waveletTimeDomain(t, max_likelihood_params[0], max_likelihood_params[1], max_likelihood_params[2], max_likelihood_params[3], max_likelihood_params[4]), c='b')
    plt.xlabel(r"time (s)")
    plt.ylabel(r"template(t)")
    plt.savefig(root_folder + figName + extension)

    n_random_draws = 10
    random_draws = np.random.randint(0, len(samples), n_random_draws)
    
    figName = "wavelets drawn from the posterior"
    plt.figure(figName)
    plt.clf()
    plt.plot(t, data, c='gray', alpha=0.7)
    plt.plot(t, wavelet, c='r')
    for i in range(n_random_draws):
        plt.plot(t, bw.waveletTimeDomain(t, samples[random_draws[i]][0], samples[random_draws[i]][1], samples[random_draws[i]][2], samples[random_draws[i]][3], samples[random_draws[i]][4]), c='b', alpha=0.5)
    plt.xlabel(r"time (s)")
    plt.ylabel(r"template(t)")
    plt.savefig(root_folder + figName + extension)


#==============================================================================
#                               DONE!
#==============================================================================


plt.show()
plt.close('all')

print("Success!")
